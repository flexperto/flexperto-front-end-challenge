import express from 'express'
import cors from 'cors';

import data from './data.json';

const app = express()
app.use(cors())
const port = 3001

app.get('/', (_, response) => {
  response.json(data);
})

app.listen(port, () => {
  console.log(`listening at http://localhost:${port}`)
})
