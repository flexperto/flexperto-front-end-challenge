export type ArchiveResponse = {
    id: string;
    createdAt: string; // ISO Date.
    conference: {
        id: string;
        tenant: string;
        participants: {
            id: string;
            email: string;
            name: string;
        }[];
    };
    navigation: {
        events: {
            target: string;
            createdAt: string; // ISO Date.
        }[];
    };
};
