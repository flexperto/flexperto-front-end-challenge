import * as React from 'react';
import logo from './logo.png';

import * as T from './types';

function App() {
    return (
        <div className="container">
            <main className="layout">
                <img className="logo" src={logo} alt="flexperto logo" />
            </main>
        </div>
    );
}

export default App;
