# Front End Challenge

## Developer Notes

1. Clone this repository, it should include everything you need to get up and
   running.
2. Once cloned, run `npm ci` in the root folder.
3. In a dedicated terminal, run `npm run server`, which will start the backend
   on `http://localhost:3001`.
4. In a dedicated terminal, run `npm start`, which will start the frontend on
   `http://localhost:3000`.
5. Start writing code in `src/App.tsx`.

## Challenge

The server on `http://localhost:3001/` should respond with an array of objects,
which you are supposed to display somehow. For each object, display its ID, the
conference ID and createdAt in the format `yyyy-mm-dd`.

## Tips

* To get the list of archives, run a `GET` request against the backend at
  `http://localhost:3001/` (at the root path).
* In `src/types.ts`, you see the shape of the objects that you get from
  the API response.
* To style your app, change the css in `src/index.css`.
* Organize the code however you want, but be mindful of the time. Don't waste
  too much time making it beautiful or fast, before it even works.
* Feel free to use any library you want to via `npm`.
